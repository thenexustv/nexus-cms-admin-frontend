import axios from 'axios'

const apiUrl = 'http://localhost:8888'

export default {
  getSeries() {
    return axios.get(apiUrl + '/api/series')
  },

  getEpisodes() {
    return axios.get(apiUrl + '/api/episodes')
  },

  getEpisode(id) {
    return axios.get(apiUrl + '/api/episodes/' + id)
  }
}
