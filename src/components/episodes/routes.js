import EpisodeList from './EpisodeList.vue'
import EpisodeEditor from './EpisodeEditor.vue'

export default [
  {
    path: '/episodes',
    name: 'Episodes',
    component: EpisodeList
  },
  {
    path: '/episode/:id(\\d+)',
    name: 'Episode Editor',
    component: EpisodeEditor
  },
  {
    path: '/episode/add',
    name: 'Add New Episode',
    component: EpisodeEditor
  }
]
