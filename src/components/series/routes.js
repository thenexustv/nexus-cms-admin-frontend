import SeriesList from './SeriesList.vue'

export default [
  {
    path: '/series',
    name: 'Series',
    component: SeriesList
  }
]
